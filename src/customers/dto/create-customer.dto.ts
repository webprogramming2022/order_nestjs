import { IsNotEmpty, Length } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  age: number;
  @IsNotEmpty()
  @Length(10)
  tel: string;
  @IsNotEmpty()
  @Length(1)
  gender: string;
}
