import { IsNotEmpty } from 'class-validator';
class CreateOrderItemsDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreateOrderItemsDto[];
}
